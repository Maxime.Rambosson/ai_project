import numpy as np

class kNN_classifier:
    def __init__(self):
        pass

    def get_distances(self, row):
        return np.linalg.norm(self.X_train - row, axis=1)

    def train(self, X, Y, learning_rate, nb_train, display_step=100):
        self.X_train = X
        self.Y_train = Y

        self.k = int(np.sqrt(X.shape[1]))

    def eval(self, X):
        results = np.array([])
        for i in range(X.shape[0]):
            distances = self.get_distances(X[i])

            sorted_index = np.argsort(distances)
            nearest_results = [self.Y_train[i][0] for i in sorted_index[0:self.k]]
            prediction = max(set(nearest_results), key=nearest_results.count)
            results = np.append(results, [prediction])

        return results