import numpy as np
import pandas as pd
import os
import matplotlib.pyplot as plt

from neural_network import Neural_Network
from sklearn.neural_network import MLPClassifier

def get_measures(Y_hat, Y_excepted):
	true_0 = 0
	false_0 = 0
	true_1 = 0
	false_1 = 0

	for i in range(len(Y_hat)):
		if Y_excepted[i] == 1:
			if Y_hat[i] == 1:
				true_1 += 1
			else:
				false_1 += 1

		else:
			if Y_hat[i] == 0:
				true_0 += 1
			else:
				false_0 += 1

	accuracy = (true_0 + true_1) / len(Y_hat)
	precision = true_1 / (true_1 + false_1)
	recall = true_1 / (true_1 + false_0)
	f1_score = 2 * (recall * precision) / (recall + precision)

	return (accuracy, precision, recall, f1_score)

def round_value(M):
	return np.uint8( M > 0.5)

df = pd.read_csv('data/agaricus-lepiota.data', sep=',', header=None)
df = pd.get_dummies(df) # Split collumns in binary covariables

df = df.drop(columns=['0_p'])
df_train = df.iloc[0:8000]
df_test = df.iloc[8000:]

Y_test = df_test["0_e"].values
X_test = df_test.drop("0_e", axis=1).values

Y_train = df_train["0_e"].values.reshape(8000, 1)
X_train = df_train.drop("0_e", axis=1).values

my_nn = Neural_Network()

hidden_layers = []

measures = [[], [], [], []]
measures_sk = [[], [], [], []]
for i in range(1, 6):
	print("== Nb layer : {} ==".format(i))
	if i > 1:
		hidden_layers.append(117)

	sk_nn = MLPClassifier(hidden_layer_sizes=tuple(hidden_layers), activation="logistic", learning_rate_init=0.01, max_iter=1000)
	sk_nn.fit(X_train, np.ravel(Y_train))

	result = round_value(sk_nn.predict(X_test))
	current_measures = get_measures(result, Y_test)

	for i in range(4):
		measures_sk[i].append(current_measures[i])

	my_nn.reset_layers()
	my_nn.add_layer(X_train.shape[1])
	my_nn.train(X_train, Y_train, 0.01, 1000, 500)

	result = round_value(my_nn.eval(X_test))
	current_measures = get_measures(result, Y_test)

	for i in range(4):
		measures[i].append(current_measures[i])

titles = ["accuracy", "precision", "recall", "f1_score"]
x_axis = np.arange(1, 6)

# Create output folder
if not os.path.exists("./results"):
	os.mkdir("./results")
# Create & save graph
for i in range(4):
	plt.plot(x_axis, measures[i])
	plt.ylabel(titles[i])
	plt.xlabel("Complexity (nb of same layer)")
	plt.savefig("./results/complexity_" + titles[i])
	plt.clf()

	plt.plot(x_axis, measures[i], label="Neural Network")
	plt.plot(x_axis, measures_sk[i], label="sklearn.neural_network.MLPClassifier")
	plt.ylabel(titles[i])
	plt.xlabel("Complexity (nb of same layer)")
	plt.legend()
	plt.savefig("./results/complexity_" + titles[i] + "_sk")
	plt.clf()
# Create file & write the raw data
with open("./results/complexity.txt", "w") as txt_file:
	txt_file.write("nb_layer ")
	for x in x_axis:
		txt_file.write(str(x) + " ")
	txt_file.write("\n")

	for i in range(4):
		txt_file.write(titles[i] + " ")
		for value in measures[i]:
			txt_file.write(str(value) + " ")
		txt_file.write("\n")