import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import os
import random

from sklearn.neural_network import MLPClassifier
from neural_network import Neural_Network
from knn_classifier import kNN_classifier


def get_measures(Y_hat, Y_excepted):
	true_0 = 0
	false_0 = 0
	true_1 = 0
	false_1 = 0

	for i in range(len(Y_hat)):
		if Y_excepted[i] == 1:
			if Y_hat[i] == 1:
				true_1 += 1
			else:
				false_1 += 1

		else:
			if Y_hat[i] == 0:
				true_0 += 1
			else:
				false_0 += 1

	accuracy = (true_0 + true_1) / len(Y_hat)
	precision = true_1 / (true_1 + false_1 + 1e-15)
	recall = true_1 / (true_1 + false_0 + 1e-15)
	f1_score = 2 * (recall * precision) / (recall + precision + 1e-15)

	return (accuracy, precision, recall, f1_score)

def round_value(M):
	return np.uint8( M > 0.5)

def get_noised_df(clean_df, noise_percent):
	noised_df = clean_df.copy()
	for _, row in noised_df.iterrows():
		for x in noised_df.columns:
			if random.random() < (noise_percent / 100):
				possibilities = clean_df[x].unique().tolist()
				possibilities.remove(row[x]) # Delete current value
				if possibilities:
					row[x] = random.choice(possibilities) # Set new value (noised)

	return noised_df


df = pd.read_csv('data/agaricus-lepiota.data', sep=',', header=None) # Complete dataset
df_dummies = pd.get_dummies(df)

df_test = df_dummies.iloc[8000:] # Dummies test dataset

Y_test = df_test["0_e"].values # Dummies Y test matrix
X_test = df_test.drop(["0_e", "0_p"], axis=1).values # Dummies X test matrix

df_train = df_dummies.iloc[0:8000] # Dummies train dataset

Y_train = df_train["0_e"].values.reshape(8000, 1) # Dummies Y train matrix
X_train = df_train.drop(["0_e", "0_p"], axis=1).values # Dummies X train matrix

my_nn = Neural_Network()
my_nn.add_layer(X_test.shape[1])

my_knn = kNN_classifier()

sk_nn = MLPClassifier(hidden_layer_sizes=(), activation="logistic", learning_rate_init=0.01, max_iter=1000)

noises_percent = [1, 5, 10, 50, 80]

measures_train_nn = [[], [], [], []]
measures_test_nn = [[], [], [], []]
measures_train_knn = [[], [], [], []]
measures_test_knn = [[], [], [], []]
measures_train_sk_nn = [[], [], [], []]
measures_test_sk_nn = [[], [], [], []]
for noise in noises_percent:
	print("== Noise {} ==".format(noise))
	df_dummies_noised = pd.get_dummies(get_noised_df(df, noise))

	df_train_noised = df_dummies_noised.iloc[0:8000]
	X_train_noised = df_train_noised.drop(["0_e", "0_p"], axis=1).values  # Dummies X train matrix noised
	Y_train_noised = df_train_noised["0_e"].values.reshape(8000, 1) # Dummies Y train matrix

	df_test_noised = df_dummies_noised.iloc[8000:]
	X_test_noised = df_test_noised.drop(["0_e", "0_p"], axis=1).values  # Dummies X test matrix noised
	Y_test_noised = df_test_noised["0_e"].values # Dummies Y test matrix

	my_nn.reset_layers()
	my_nn.train(X_train_noised, Y_train_noised, 0.01, 1000, 500)
	result = round_value(my_nn.eval(X_test))
	current_measures = get_measures(result, Y_test)

	for i in range(4):
		measures_train_nn[i].append(current_measures[i])

	my_knn.train(X_train_noised, Y_train_noised, 0, 0)
	result = my_knn.eval(X_test)
	current_measures = get_measures(result, Y_test)

	for i in range(4):
		measures_train_knn[i].append(current_measures[i])

	sk_nn.fit(X_train_noised, np.ravel(Y_train_noised))
	result = round_value(sk_nn.predict(X_test))
	current_measures = get_measures(result, Y_test)

	for i in range(4):
		measures_train_sk_nn[i].append(current_measures[i])

	my_nn.reset_layers()
	my_nn.train(X_train, Y_train, 0.01, 1000, 500)
	result = round_value(my_nn.eval(X_test_noised))
	current_measures = get_measures(result, Y_test)

	for i in range(4):
		measures_test_nn[i].append(current_measures[i])

	my_knn.train(X_train, Y_train, 0, 0)
	result = my_knn.eval(X_test_noised)
	current_measures = get_measures(result, Y_test)

	for i in range(4):
		measures_test_knn[i].append(current_measures[i])

	sk_nn.fit(X_train, np.ravel(Y_train))
	result = round_value(sk_nn.predict(X_test_noised))
	current_measures = get_measures(result, Y_test)

	for i in range(4):
		measures_test_sk_nn[i].append(current_measures[i])

		
titles = ["accuracy", "precision", "recall", "f1_score"]
		
# Create output folder
if not os.path.exists("./results"):
	os.mkdir("./results")

# Create & save graph noise
for i in range(4):
	plt.plot(noises_percent, measures_train_nn[i], label="Neural Network")
	plt.plot(noises_percent, measures_train_knn[i], label="k-nn classifier")
	plt.ylabel(titles[i])
	plt.xlabel("Noises (in percent)")
	plt.legend()
	plt.savefig("./results/noise_train_" + titles[i])
	plt.clf()

	plt.plot(noises_percent, measures_train_nn[i], label="Neural Network")
	plt.plot(noises_percent, measures_train_knn[i], label="k-nn classifier")
	plt.plot(noises_percent, measures_train_sk_nn[i], label="sklearn.neural_network.MLPClassifier")
	plt.ylabel(titles[i])
	plt.xlabel("Noises (in percent)")
	plt.legend()
	plt.savefig("./results/noise_train_" + titles[i] + "_sk")
	plt.clf()

	plt.plot(noises_percent, measures_test_nn[i], label="Neural Network")
	plt.plot(noises_percent, measures_test_knn[i], label="k-nn classifier")
	plt.ylabel(titles[i])
	plt.xlabel("Noises (in percent)")
	plt.legend()
	plt.savefig("./results/noise_test_" + titles[i])
	plt.clf()

	plt.plot(noises_percent, measures_test_nn[i], label="Neural Network")
	plt.plot(noises_percent, measures_test_knn[i], label="k-nn classifier")
	plt.plot(noises_percent, measures_test_sk_nn[i], label="sklearn.neural_network.MLPClassifier")
	plt.ylabel(titles[i])
	plt.xlabel("Noises (in percent)")
	plt.legend()
	plt.savefig("./results/noise_test_" + titles[i] + "_sk")
	plt.clf()
	
# Create file & write the raw data train noise
with open("./results/noise_train.txt", "w") as txt_file:
	txt_file.write("noise_percent ")
	for x in noises_percent:
		txt_file.write(str(x) + " ")
	txt_file.write("\n")

	for i in range(4):
		txt_file.write(titles[i] + " ")
		for value in measures_train_nn[i]:
			txt_file.write(str(value) + " ")
		txt_file.write("\n")

# Create file & write the raw data test noise
with open("./results/noise_test.txt", "w") as txt_file:
	txt_file.write("noise_percent ")
	for x in noises_percent:
		txt_file.write(str(x) + " ")
	txt_file.write("\n")

	for i in range(4):
		txt_file.write(titles[i] + " ")
		for value in measures_test_nn[i]:
			txt_file.write(str(value) + " ")
		txt_file.write("\n")