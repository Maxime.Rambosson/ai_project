import numpy as np
import pandas as pd
import os
import matplotlib.pyplot as plt

from neural_network import Neural_Network
from knn_classifier import kNN_classifier
from sklearn.neural_network import MLPClassifier

def get_measures(Y_hat, Y_excepted):
	true_0 = 0
	false_0 = 0
	true_1 = 0
	false_1 = 0

	for i in range(len(Y_hat)):
		if Y_excepted[i] == 1:
			if Y_hat[i] == 1:
				true_1 += 1
			else:
				false_1 += 1

		else:
			if Y_hat[i] == 0:
				true_0 += 1
			else:
				false_0 += 1

	accuracy = (true_0 + true_1) / len(Y_hat)
	precision = true_1 / (true_1 + false_1)
	recall = true_1 / (true_1 + false_0)
	f1_score = 2 * (recall * precision) / (recall + precision)

	return (accuracy, precision, recall, f1_score)

def round_value(M):
	return np.uint8( M > 0.5)

df = pd.read_csv('data/agaricus-lepiota.data', sep=',', header=None)
df = pd.get_dummies(df) # Split collumns in binary covariables

df = df.drop(columns=['0_p'])
df_test = df.iloc[8000:]

Y_test = df_test["0_e"].values
X_test = df_test.drop("0_e", axis=1).values

my_nn = Neural_Network()
my_nn.add_layer(X_test.shape[1])

my_knn = kNN_classifier()
sk_nn = MLPClassifier(hidden_layer_sizes=(), activation="logistic", learning_rate_init=0.01, max_iter=1000)

measures_nn = [[], [], [], []]
measures_knn = [[], [], [], []]
measures_sk_nn = [[], [], [], []]
for i in range(1000, 9000, 1000):
	print("== Nb data train : {} ==".format(i))
	df_train = df.iloc[0:i]

	Y_train = df_train["0_e"].values.reshape(i, 1)
	X_train = df_train.drop("0_e", axis=1).values

	my_nn.reset_layers()
	my_nn.train(X_train, Y_train, 0.01, 1000, 500)

	result = round_value(my_nn.eval(X_test))
	current_measures = get_measures(result, Y_test)

	for i in range(4):
		measures_nn[i].append(current_measures[i])

	my_knn.train(X_train, Y_train, 0, 0)

	result = my_knn.eval(X_test)
	current_measures = get_measures(result, Y_test)

	for i in range(4):
		measures_knn[i].append(current_measures[i])

	sk_nn.fit(X_train, np.ravel(Y_train))

	result = round_value(sk_nn.predict(X_test))
	current_measures = get_measures(result, Y_test)

	for i in range(4):
		measures_sk_nn[i].append(current_measures[i])


x_axis = np.arange(1000, 9000, 1000)
titles = ["accuracy", "precision", "recall", "f1_score"]

# Create output folder
if not os.path.exists("./results"):
	os.mkdir("./results")
# Create & save graph
for i in range(4):
	plt.plot(x_axis, measures_nn[i], label="Neural Network")
	plt.plot(x_axis, measures_knn[i], label="k-nn classifier")
	plt.ylabel(titles[i])
	plt.xlabel("Nb data used for training")
	plt.legend()
	plt.savefig("./results/quantity_" + titles[i])
	plt.clf()


	plt.plot(x_axis, measures_nn[i], label="Neural Network")
	plt.plot(x_axis, measures_knn[i], label="k-nn classifier")
	plt.plot(x_axis, measures_sk_nn[i], label="sklearn.neural_network.MLPClassifier")
	plt.ylabel(titles[i])
	plt.xlabel("Nb data used for training")
	plt.legend()
	plt.savefig("./results/quantity_" + titles[i] + "_sk")
	plt.clf()

# Create file & write the raw data
with open("./results/quantity.txt", "w") as txt_file:
	txt_file.write("nb_data ")
	for x in x_axis:
		txt_file.write(str(x) + " ")
	txt_file.write("\n")

	for i in range(4):
		txt_file.write(titles[i] + " ")
		for value in measures_nn[i]:
			txt_file.write(str(value) + " ")
		txt_file.write("\n")
