import numpy as np
import pandas as pd

def sigmoid(Z):
	return 1 / (1 + np.exp(-Z))

def mean_squared_error(y_pred, y_true):
	return ((y_pred - y_true)**2).sum() / (2*y_pred.size)

class Neural_Network:
	def __init__(self):
		self.Ws = [] # List of weights matrix

	def add_layer(self, nb_node):
		if self.Ws:
			self.Ws[-1] = 0.01 * np.random.randn(self.Ws[-1].shape[0], nb_node + 1)

		self.Ws.append(0.01 * np.random.randn(nb_node + 1, 1))

	def reset_layers(self):
		for i in range(len(self.Ws)):
			self.Ws[i] = 0.01 * np.random.randn(self.Ws[i].shape[0], self.Ws[i].shape[1])
		
	def train(self, X, Y, learning_rate, nb_train, display_step=100):
		X = np.c_[X, np.ones((X.shape[0]))]

		if not self.Ws:
			print("NN training : No layer")
			return None

		if X.shape[0] != Y.shape[0]:
			print("NN training : X.shape[0] doesn't match Y.shape[0]")
			return None

		if X.shape[1] != self.Ws[0].shape[0]:
			print("NN training : X.shape[1] doesn't match first layer shape")
			return None

		results = pd.DataFrame(columns=["mse"])
		error = np.empty_like(Y)
		for epoch in range(nb_train):
			A = [X]

			for layer in range(len(self.Ws)):
				net = np.dot(A[layer], self.Ws[layer])

				out = sigmoid(net)

				A.append(out)
	
			mse = mean_squared_error(A[-1], Y)
			results = results.append({"mse":mse}, ignore_index=True)
			if epoch % display_step == 0:
				print("[NN info]", mse)

			error = A[-1] - Y
			D = [ error * A[-1] * (1 - A[-1]) ]

			for layer in range(len(A) - 2, 0, -1):
				E = np.dot(D[-1], self.Ws[layer].T)
				delta = E * A[layer] * (1 - A[layer])

				D.append(delta)

			D = D[::-1]

			for layer in range(len(self.Ws)):
				self.Ws[layer] += -learning_rate * np.dot(A[layer].T, D[layer]) / Y.size

		return results

	def eval(self, X):
		X = np.c_[X, np.ones((X.shape[0]))]
		if not self.Ws:
			print("NN Evaluation : No layer")
			return None

		if X.shape[1] != self.Ws[0].shape[0]:
			print("NN Evaluation : X.shape[1] doesn't match first layer shape")
			return None

		output = X
		for w in self.Ws:
			output = sigmoid(np.dot(output, w))

		return output

	def __repr__(self) -> str:
		if not self.Ws:
			return "Empty"

		representation = str(self.Ws[0].shape[0])
		for i in range(len(self.Ws)):
			representation += "-" + str(self.Ws[i].shape[1])

		return representation