import numpy as np
import pandas as pd
import os
import matplotlib.pyplot as plt

from neural_network import Neural_Network
from sklearn.neural_network import MLPClassifier

def get_measures(Y_hat, Y_excepted):
	true_0 = 0
	false_0 = 0
	true_1 = 0
	false_1 = 0

	for i in range(len(Y_hat)):
		if Y_excepted[i] == 1:
			if Y_hat[i] == 1:
				true_1 += 1
			else:
				false_1 += 1

		else:
			if Y_hat[i] == 0:
				true_0 += 1
			else:
				false_0 += 1

	accuracy = (true_0 + true_1) / len(Y_hat)
	precision = true_1 / (true_1 + false_1 + 1e-15)
	recall = true_1 / (true_1 + false_0 + 1e-15)
	f1_score = 2 * (recall * precision) / (recall + precision + 1e-15)

	return (accuracy, precision, recall, f1_score)

def round_value(M):
	return np.uint8( M > 0.5)

df = pd.read_csv('data/agaricus-lepiota.data', sep=',', header=None)
df = pd.get_dummies(df.drop(columns=[5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22])) # Split collumns in binary covariables

df = df.drop(columns=['0_p'])
df_train = df.iloc[0:10]
df_test = df.iloc[4000:]

Y_test = df_test["0_e"].values
X_test = df_test.drop("0_e", axis=1).values

Y_train = df_train["0_e"].values.reshape(df_train["0_e"].shape[0], 1)
X_train = df_train.drop("0_e", axis=1).values

my_neural_network = Neural_Network()
my_neural_network.add_layer(X_train.shape[1])

nb_trains = [100, 1000, 5000, 10000, 50000]
measures = [[], [], [], []]
measures_sk = [[], [], [], []]
for train in nb_trains:
	print("== Nb train : {} ==".format(train))
	my_neural_network.reset_layers()
	my_neural_network.train(X_train, Y_train, 0.01, train, train)

	current_measures = get_measures(round_value(my_neural_network.eval(X_test)), Y_test)

	for i in range(4):
		measures[i].append(current_measures[i])

	sk_nn = MLPClassifier(hidden_layer_sizes=(), activation="logistic", learning_rate_init=0.01, max_iter=train, n_iter_no_change=train)
	sk_nn.fit(X_train, np.ravel(Y_train))

	current_measures = get_measures(round_value(my_neural_network.eval(X_test)), Y_test)

	for i in range(4):
		measures_sk[i].append(current_measures[i])



titles = ["accuracy", "precision", "recall", "f1_score"]
# Create output folder
if not os.path.exists("./results"):
	os.mkdir("./results")
# Create & save graph
for i in range(4):
	plt.plot(nb_trains, measures[i])
	plt.ylabel(titles[i])
	plt.xlabel("Nb of train")
	plt.savefig("./results/overfitting_" + titles[i])
	plt.clf()

	plt.plot(nb_trains, measures[i], label="Neural Network")
	plt.plot(nb_trains, measures_sk[i], label="sklearn.neural_network.MLPClassifier")
	plt.ylabel(titles[i])
	plt.xlabel("Nb of train")
	plt.legend()
	plt.savefig("./results/overfitting_" + titles[i] + "_sk")
	plt.clf()

# Create file & write the raw data
with open("./results/overfitting.txt", "w") as txt_file:
	txt_file.write("nb_train ")
	for x in nb_trains:
		txt_file.write(str(x) + " ")
	txt_file.write("\n")

	for i in range(4):
		txt_file.write(titles[i] + " ")
		for value in measures[i]:
			txt_file.write(str(value) + " ")
		txt_file.write("\n")