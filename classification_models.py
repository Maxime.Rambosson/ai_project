import pandas as pd
import matplotlib.pyplot as plt
import os

from neural_network import Neural_Network

df = pd.read_csv('data/agaricus-lepiota.data', sep=',', header=None)
df = pd.get_dummies(df) # Split reference collumn in two binary covariables

df = df.drop(columns=['0_p'])
df_train = df.iloc[0:8000]

Y_train = df_train["0_e"].values.reshape(8000, 1)
X_train = df_train.drop("0_e", axis=1).values

models = []

my_neural_network = Neural_Network()
my_neural_network.add_layer(117)
models.append(my_neural_network)

my_neural_network = Neural_Network()
my_neural_network.add_layer(117)
my_neural_network.add_layer(2)
models.append(my_neural_network)

my_neural_network = Neural_Network()
my_neural_network.add_layer(117)
my_neural_network.add_layer(117)
models.append(my_neural_network)

my_neural_network = Neural_Network()
my_neural_network.add_layer(117)
my_neural_network.add_layer(2)
my_neural_network.add_layer(2)
models.append(my_neural_network)

training_results = [ model.train(X_train, Y_train, 0.5, 1000, 500) for model in models ]

if not os.path.exists("./results"):
	os.mkdir("./results")
for i in range(len(training_results)):
	plt.clf()
	training_results[i].mse.plot(title="Mean Squared Error : {}".format(models[i]))
	plt.savefig("results/{}".format(models[i]))

for w in models[-1].Ws:
	print(w)