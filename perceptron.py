import numpy as np
import pandas as pd

import sklearn.linear_model as sklm

from neural_network import Neural_Network

def round_value(M):
    return np.uint8( M > 0.5)

def print_measures(Y_hat, Y_excepted):
	true_0 = 0
	false_0 = 0
	true_1 = 0
	false_1 = 0

	for i in range(len(Y_hat)):
		if Y_excepted[i] == 1:
			if Y_hat[i] == 1:
				true_1 += 1
			else:
				false_1 += 1

		else:
			if Y_hat[i] == 0:
				true_0 += 1
			else:
				false_0 += 1

	print("Accuracy:", (true_0 + true_1) / len(Y_hat))
	precision = true_1 / (true_1 + false_1)
	print("Precision:", precision)
	recall = true_1 / (true_1 + false_0)
	print("Recall:", recall)
	print("F1 score:", 2 * (recall * precision) / (recall + precision))

df = pd.read_csv('data/agaricus-lepiota.data', sep=',', header=None)

df = pd.get_dummies(df) # Split reference collumn in binary covariables
df = df.drop(columns=['0_p'])

df_train = df.iloc[0:8000]
df_test = df.iloc[8000:]

Y_train = df_train["0_e"].values.reshape(8000, 1)
X_train = df_train.drop("0_e", axis=1).values

Y_test = df_test["0_e"].values
X_test = df_test.drop("0_e", axis=1).values

my_nn = Neural_Network()
my_nn.add_layer(X_train.shape[1])
my_nn.train(X_train, Y_train, 0.01, 1000)

result = my_nn.eval(X_test)
result = round_value(result)

clf = sklm.Perceptron(tol=1e-3, shuffle=False)
clf.fit(X_train, np.ravel(Y_train))
result2 = clf.predict(X_test)

print("== Own Neural Network ==")
print_measures(result, Y_test)
print("== Sklearn ==")
print_measures(result2, Y_test)